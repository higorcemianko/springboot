package com.example.springboot.services;

import com.example.springboot.models.ProdutoModel;
import com.example.springboot.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductsService {

    @Autowired
    ProdutoRepository produtoReposotory;

    public List<ProdutoModel> findAll(){
        return produtoReposotory.findAll();
    }

    public ProdutoModel deleteById(long id){
        Optional<ProdutoModel> produto = produtoReposotory.findById(id);
        if (!produto.isPresent()){
            return null;
        } else {
            produtoReposotory.delete(produto.get());
            return produto.get();
        }
    }

    public ProdutoModel findById(long id){
        Optional<ProdutoModel> produto = produtoReposotory.findById(id);
        if (!produto.isPresent()){
            return null;
        } else {
            return produto.get();
        }
    }

    public ProdutoModel save(ProdutoModel produtoModel) {
        return produtoReposotory.save(produtoModel);
    }
}
