package com.example.springboot.controllers;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.example.springboot.services.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.springboot.models.ProdutoModel;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200", "http://localhost:8081" })
@RestController
public class ProdutoController{
    @Autowired
    private ProductsService productsService;

    @GetMapping("/produtos")
    public List<ProdutoModel> getAllProdutos(){
    	/* HTTP REST PATTERN
    	/*List<ProdutoModel> produtosList = produtoReposotory.findAll();
    	if (produtosList.isEmpty()) {
    		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    	} else {
    		return new ResponseEntity<List<ProdutoModel>>(produtosList, HttpStatus.OK);
    	}*/
        return productsService.findAll();
    }
    
    @GetMapping("/produtos/{id}")
    public ProdutoModel getOneProduto(@PathVariable(value="id") long id){
    	return productsService.findById(id);
    }

    @DeleteMapping("/produtos/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable long id){
        ProdutoModel produtoModel = productsService.deleteById(id);
        if (Objects.nonNull(produtoModel)){
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/produtos/{id}")
    public ResponseEntity<ProdutoModel> updateProduct(@PathVariable long id, @RequestBody ProdutoModel produtoModel){
        ProdutoModel productUpdated = productsService.save(produtoModel);
        return new ResponseEntity<ProdutoModel>(productUpdated, HttpStatus.OK);
    }

    @PostMapping("/produtos")
    public ResponseEntity<ProdutoModel> saveProduct(@RequestBody ProdutoModel produtoModel){
        ProdutoModel newProdutoModel = productsService.save(produtoModel);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(newProdutoModel.getIdProduto()).toUri();

        return new ResponseEntity<ProdutoModel>(newProdutoModel, HttpStatus.CREATED);

    }


}